# OneParty

# Komendy OP

Gdy lider party użyje tych komend - automatycznie każdy członek je też używa w tym samym czasie.
Ustawiamy je w linijce 14.
Możemy maksymalnie użyc 5 komend.

# MySql

W pierwszych linjkach kodu jest coś jak:
`script options: #Opcje MySql
    $ init com.mysql.jdbc.Driver
    $ db url url do bazy
    $ db username user
    $ db password haslo`
Musisz to prawidłowo skonfigurować i poda te dane na wsyzstkich serwerach.
W '$ init com.mysql.jdbc.Driver' **NIC NIE ZMIENIAJ!**
W '$ db url url do bazy' podaj url do bazy np. mysql.titanaxe.com
W '$ db username user' podaj nazwe użytkownika do bazy.
W '$ db password haslo' podaj haslo do bazy.
